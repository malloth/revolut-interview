package com.revolut.interview.screen.converter.viewmodel

import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.revolut.interview.domain.rates.fetcher.RatesFetcher
import com.revolut.interview.domain.rates.model.ConversionRates
import com.revolut.interview.screen.converter.model.ConversionData
import org.junit.Test
import java.util.*

class ConverterViewModelTest {

    private val baseValueDataMock: MutableLiveData<String> = mock()
    private val conversionDataMock: MutableLiveData<List<ConversionData>> = mock()
    private val ratesFetcherMock: RatesFetcher = mock()

    private val tested = ConverterViewModel(
        baseValueDataMock,
        conversionDataMock,
        ratesFetcherMock,
        Locale.US
    )

    @Test
    fun `starts fetching conversion rates when created`() {
        verify(ratesFetcherMock).startFetching()
    }

    @Test
    fun `sets base value when conversion is clicked`() {
        val conversions = listOf(
            ConversionData("USD", "111"),
            ConversionData("PLN", "222"),
            ConversionData("EUR", "333")
        )
        val clickedConversion = ConversionData("EUR", "333")

        whenever(conversionDataMock.value).doReturn(conversions)

        tested.onConversionClicked(clickedConversion)

        verify(baseValueDataMock).value = clickedConversion.value
    }

    @Test
    fun `sets fetcher's base when conversion is clicked`() {
        val conversions = listOf(
            ConversionData("USD", "111"),
            ConversionData("PLN", "222"),
            ConversionData("EUR", "333")
        )
        val clickedConversion = ConversionData("EUR", "333")

        whenever(conversionDataMock.value).doReturn(conversions)

        tested.onConversionClicked(clickedConversion)

        verify(ratesFetcherMock).setBase(clickedConversion.currency)
    }

    @Test
    fun `inserts conversion at the beginning of list when it is clicked`() {
        val conversions = listOf(
            ConversionData("USD", "111"),
            ConversionData("PLN", "222"),
            ConversionData("EUR", "333")
        )
        val clickedConversion = ConversionData("EUR", "333")

        whenever(conversionDataMock.value).doReturn(conversions)

        tested.onConversionClicked(clickedConversion)

        verify(conversionDataMock).postValue(
            listOf(
                ConversionData("EUR", "333"),
                ConversionData("USD", "111"),
                ConversionData("PLN", "222")
            )
        )
    }

    @Test
    fun `sets base value when its value has changed`() {
        val conversions = listOf(
            ConversionData("USD", "0"),
            ConversionData("PLN", "0"),
            ConversionData("EUR", "0")
        )
        val conversionRates = ConversionRates(
            base = "USD",
            date = "",
            rates = mapOf(
                "PLN" to 2.0,
                "EUR" to 4.0
            )
        )
        val input = "1"

        whenever(baseValueDataMock.value).doReturn("0")
        whenever(conversionDataMock.value).doReturn(conversions)
        whenever(ratesFetcherMock.base).doReturn(mock())
        whenever(ratesFetcherMock.base.value).doReturn("USD")
        whenever(ratesFetcherMock.rates).doReturn(mock())
        whenever(ratesFetcherMock.rates.value).doReturn(conversionRates)

        tested.onInputTextChanged(input)

        verify(baseValueDataMock).value = input
    }

    @Test
    fun `updates conversion values when base value has changed`() {
        val conversions = listOf(
            ConversionData("USD", "0"),
            ConversionData("PLN", "0"),
            ConversionData("EUR", "0")
        )
        val conversionRates = ConversionRates(
            base = "USD",
            date = "",
            rates = mapOf(
                "PLN" to 2.0,
                "EUR" to 4.0
            )
        )
        val input = "1"

        whenever(baseValueDataMock.value).doReturn(input)
        whenever(conversionDataMock.value).doReturn(conversions)
        whenever(ratesFetcherMock.base).doReturn(mock())
        whenever(ratesFetcherMock.base.value).doReturn("USD")
        whenever(ratesFetcherMock.rates).doReturn(mock())
        whenever(ratesFetcherMock.rates.value).doReturn(conversionRates)

        tested.onInputTextChanged(input)

        verify(conversionDataMock).postValue(
            listOf(
                ConversionData("USD", input),
                ConversionData("PLN", "2.00"),
                ConversionData("EUR", "4.00")
            )
        )
    }

    @Test
    fun `updates conversion values when conversion rates have changed`() {
        val conversions = listOf(
            ConversionData("USD", "1"),
            ConversionData("PLN", "2"),
            ConversionData("EUR", "3")
        )
        val conversionRates = ConversionRates(
            base = "USD",
            date = "",
            rates = mapOf(
                "PLN" to 2.0,
                "EUR" to 4.0
            )
        )

        whenever(baseValueDataMock.value).doReturn("1")
        whenever(conversionDataMock.value).doReturn(conversions)
        whenever(ratesFetcherMock.base).doReturn(mock())
        whenever(ratesFetcherMock.base.value).doReturn("USD")
        whenever(ratesFetcherMock.rates).doReturn(mock())
        whenever(ratesFetcherMock.rates.value).doReturn(conversionRates)

        tested.onConversionRatesChanged()

        verify(conversionDataMock).postValue(
            listOf(
                ConversionData("USD", "1"),
                ConversionData("PLN", "2.00"),
                ConversionData("EUR", "4.00")
            )
        )
    }
}