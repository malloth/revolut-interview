package com.revolut.interview.extensions

import android.text.TextWatcher
import android.widget.EditText
import com.nhaarman.mockitokotlin2.*
import com.revolut.interview.R
import org.junit.Test

class EditTextTest {

    @Test
    fun `removes previous TextWatcher if was set in tag`() {
        val editText: EditText = mock {
            on { getTag(R.id.edit_text_listener) } doReturn mock<TextWatcher>()
        }

        editText.setTextChangedListener(mock())

        verify(editText).removeTextChangedListener(any())
    }

    @Test
    fun `does not remove previous TextWatcher if tag was not an instance of a TextWatcher`() {
        val editText: EditText = mock {
            on { getTag(R.id.edit_text_listener) } doReturn mock()
        }

        editText.setTextChangedListener(mock())

        verify(editText, never()).removeTextChangedListener(any())
    }

    @Test
    fun `does not remove previous TextWatcher if tag was null`() {
        val editText: EditText = mock {
            on { getTag(R.id.edit_text_listener) } doReturn null
        }

        editText.setTextChangedListener(mock())

        verify(editText, never()).removeTextChangedListener(any())
    }

    @Test
    fun `does not add TextWatcher when given listener is null`() {
        val listener: ((String) -> Unit)? = null
        val editText: EditText = mock {
            on { getTag(R.id.edit_text_listener) } doReturn null
        }

        editText.setTextChangedListener(listener)

        verify(editText, never()).addTextChangedListener(any())
    }

    @Test
    fun `does not set TextWatcher as a tag when given listener is null`() {
        val listener: ((String) -> Unit)? = null
        val editText: EditText = mock {
            on { getTag(R.id.edit_text_listener) } doReturn null
        }

        editText.setTextChangedListener(listener)

        verify(editText, never()).setTag(any(), any())
    }

    @Test
    fun `adds TextWatcher when given listener is not null`() {
        val listener: ((String) -> Unit)? = mock()
        val editText: EditText = mock {
            on { getTag(R.id.edit_text_listener) } doReturn null
        }

        editText.setTextChangedListener(listener)

        verify(editText).addTextChangedListener(any())
    }

    @Test
    fun `sets TextWatcher as a tag when given listener is not null`() {
        val listener: ((String) -> Unit)? = mock()
        val editText: EditText = mock {
            on { getTag(R.id.edit_text_listener) } doReturn null
        }

        editText.setTextChangedListener(listener)

        verify(editText).setTag(eq(R.id.edit_text_listener), any<TextWatcher>())
    }
}