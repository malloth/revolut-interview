package com.revolut.interview.extensions

import androidx.lifecycle.LiveData
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class LiveDataTest {

    @Test
    fun `returns value when value is not null`() {
        val expected = 123
        val liveDataMock: LiveData<Int> = mock {
            on { value } doReturn expected
        }

        val result = liveDataMock.requireValue()

        assertEquals(expected, result)
    }

    @Test(expected = IllegalStateException::class)
    fun `throws IllegalStateException when value is null`() {
        val liveDataMock: LiveData<Any> = mock {
            on { value } doReturn null
        }

        liveDataMock.requireValue()
    }
}