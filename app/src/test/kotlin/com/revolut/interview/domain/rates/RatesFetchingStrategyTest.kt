package com.revolut.interview.domain.rates

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.*
import com.revolut.interview.domain.rates.api.RatesApi
import com.revolut.interview.domain.rates.fetcher.RatesFetchingStrategy
import com.revolut.interview.domain.rates.model.ConversionRates
import okhttp3.ResponseBody
import org.junit.Test
import retrofit2.Call
import retrofit2.Response

class RatesFetchingStrategyTest {

    private val apiMock: RatesApi = mock()
    private val baseDataMock: LiveData<String> = mock()
    private val ratesDataMock: MutableLiveData<ConversionRates> = mock()

    private val tested = RatesFetchingStrategy

    @Test
    fun `posts conversion rates when fetched successfully`() {
        val base = "EUR"
        val conversionRates = ConversionRates.EMPTY
        val callMock: Call<ConversionRates> = mock {
            on { execute() } doReturn Response.success(conversionRates)
        }
        whenever(baseDataMock.value).thenReturn(base)
        whenever(apiMock.getLatestRates(eq(base))).thenReturn(callMock)

        tested.invoke(apiMock, baseDataMock, ratesDataMock)

        verify(ratesDataMock).postValue(conversionRates)
    }

    @Test
    fun `does not post conversion rates when error occurred during fetching`() {
        val base = "EUR"
        val callMock: Call<ConversionRates> = mock {
            on { execute() } doReturn Response.error(404, ResponseBody.create(null, ""))
        }
        whenever(baseDataMock.value).thenReturn(base)
        whenever(apiMock.getLatestRates(eq(base))).thenReturn(callMock)

        tested.invoke(apiMock, baseDataMock, ratesDataMock)

        verify(ratesDataMock, never()).postValue(any())
    }
}