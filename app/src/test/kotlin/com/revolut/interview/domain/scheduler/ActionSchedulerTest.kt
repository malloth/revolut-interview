package com.revolut.interview.domain.scheduler

import com.nhaarman.mockitokotlin2.*
import org.junit.Test
import java.util.*

class ActionSchedulerTest {

    private val timerMock: Timer = mock()
    private val timerTaskFactoryMock: (() -> Unit) -> TimerTask = mock()

    private val tested = ActionScheduler(timerMock, timerTaskFactoryMock)

    @Test
    fun `schedules task at a fixed rate`() {
        val frequency = 1_000L
        val actionMock: () -> Unit = mock()
        val taskMock: TimerTask = mock()

        whenever(timerTaskFactoryMock.invoke(any())).doReturn(taskMock)

        tested.scheduleRecurrent(frequency, actionMock)

        verify(timerMock).scheduleAtFixedRate(taskMock, 0L, frequency)
        verifyNoMoreInteractions(timerMock)
    }

    @Test
    fun `cancels all tasks`() {
        tested.unscheduleAll()

        verify(timerMock).cancel()
        verifyNoMoreInteractions(timerMock)
    }
}