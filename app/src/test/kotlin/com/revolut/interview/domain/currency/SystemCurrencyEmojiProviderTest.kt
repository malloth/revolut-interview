package com.revolut.interview.domain.currency

import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class SystemCurrencyEmojiProviderTest(
    private val currency: String,
    private val emoji: String,
    private val exception: Class<out Exception>?
) {
    @get:Rule
    val exceptionRule: ExpectedException = ExpectedException.none()

    private val tested = SystemCurrencyEmojiProvider

    @Test
    fun `returns emoji flag for currency`() {
        if (exception != null) {
            exceptionRule.expect(exception)
        }
        assertEquals(emoji, tested.getCurrencyEmoji(currency))
    }

    companion object {

        @JvmStatic
        @Parameterized.Parameters(name = "returns valid emoji flag for currency: {0}")
        fun data(): Iterable<Array<Any?>> =
            listOf<Array<Any?>>(
                arrayOf("AUD", "\uD83C\uDDE6\uD83C\uDDFA", null),
                arrayOf("BGN", "\uD83C\uDDE7\uD83C\uDDEC", null),
                arrayOf("BRL", "\uD83C\uDDE7\uD83C\uDDF7", null),
                arrayOf("CAD", "\uD83C\uDDE8\uD83C\uDDE6", null),
                arrayOf("CHF", "\uD83C\uDDF1\uD83C\uDDEE", null),
                arrayOf("CNY", "\uD83C\uDDE8\uD83C\uDDF3", null),
                arrayOf("CZK", "\uD83C\uDDE8\uD83C\uDDFF", null),
                arrayOf("DKK", "\uD83C\uDDE9\uD83C\uDDF0", null),
                arrayOf("EUR", "\uD83C\uDDEA\uD83C\uDDFA", null),
                arrayOf("GBP", "\uD83C\uDDEC\uD83C\uDDE7", null),
                arrayOf("HKD", "\uD83C\uDDED\uD83C\uDDF0", null),
                arrayOf("HRK", "\uD83C\uDDED\uD83C\uDDF7", null),
                arrayOf("HUF", "\uD83C\uDDED\uD83C\uDDFA", null),
                arrayOf("IDR", "\uD83C\uDDEE\uD83C\uDDE9", null),
                arrayOf("ILS", "\uD83C\uDDEE\uD83C\uDDF1", null),
                arrayOf("INR", "\uD83C\uDDEE\uD83C\uDDF3", null),
                arrayOf("ISK", "\uD83C\uDDEE\uD83C\uDDF8", null),
                arrayOf("JPY", "\uD83C\uDDEF\uD83C\uDDF5", null),
                arrayOf("KRW", "\uD83C\uDDF0\uD83C\uDDF7", null),
                arrayOf("MXN", "\uD83C\uDDF2\uD83C\uDDFD", null),
                arrayOf("MYR", "\uD83C\uDDF2\uD83C\uDDFE", null),
                arrayOf("NOK", "\uD83C\uDDF3\uD83C\uDDF4", null),
                arrayOf("NZD", "\uD83C\uDDF3\uD83C\uDDFF", null),
                arrayOf("PHP", "\uD83C\uDDF5\uD83C\uDDED", null),
                arrayOf("PLN", "\uD83C\uDDF5\uD83C\uDDF1", null),
                arrayOf("RON", "\uD83C\uDDF7\uD83C\uDDF4", null),
                arrayOf("RUB", "\uD83C\uDDF7\uD83C\uDDFA", null),
                arrayOf("SEK", "\uD83C\uDDF8\uD83C\uDDEA", null),
                arrayOf("SGD", "\uD83C\uDDF8\uD83C\uDDEC", null),
                arrayOf("THB", "\uD83C\uDDF9\uD83C\uDDED", null),
                arrayOf("TRY", "\uD83C\uDDF9\uD83C\uDDF7", null),
                arrayOf("USD", "\uD83C\uDDFA\uD83C\uDDF8", null),
                arrayOf("ZAR", "\uD83C\uDDFF\uD83C\uDDE6", null),
                arrayOf("XXX", "", IllegalArgumentException::class.java),
                arrayOf("", "", IllegalArgumentException::class.java)
            )
    }
}