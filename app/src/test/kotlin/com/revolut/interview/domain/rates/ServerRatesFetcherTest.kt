package com.revolut.interview.domain.rates

import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.revolut.interview.domain.rates.api.RatesApi
import com.revolut.interview.domain.rates.fetcher.ServerRatesFetcher
import com.revolut.interview.domain.rates.model.ConversionRates
import com.revolut.interview.domain.scheduler.Scheduler
import org.junit.Test

class ServerRatesFetcherTest {

    private val apiMock: RatesApi = mock()
    private val schedulerMock: Scheduler = mock()
    private val baseDataMock: MutableLiveData<String> = mock()
    private val ratesDataMock: MutableLiveData<ConversionRates> = mock()
    private val ratesFetchingTaskMock: () -> Unit = mock()

    private val tested = ServerRatesFetcher(
        apiMock,
        schedulerMock,
        baseDataMock,
        ratesDataMock,
        ratesFetchingTaskMock
    )

    @Test
    fun `schedules recurrent task to repeat each second when starts fetching`() {
        tested.startFetching()

        verify(schedulerMock).scheduleRecurrent(FETCH_FREQUENCY_MS, ratesFetchingTaskMock)
    }

    @Test
    fun `unschedules all tasks when stops fetching`() {
        tested.stopFetching()

        verify(schedulerMock).unscheduleAll()
    }

    @Test
    fun `posts new base value when it is set`() {
        val currency = "EUR"

        tested.setBase(currency)

        verify(baseDataMock).postValue(currency)
    }

    private companion object {

        const val FETCH_FREQUENCY_MS = 1_000L
    }
}