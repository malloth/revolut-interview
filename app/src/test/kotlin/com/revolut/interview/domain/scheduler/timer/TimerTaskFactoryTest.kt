package com.revolut.interview.domain.scheduler.timer

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import org.junit.Test

class TimerTaskFactoryTest {

    private val tested = TimerTaskFactory

    @Test
    fun `created task which run given action`() {
        val action: () -> Unit = mock()

        val result = tested.create(action)
        result.run()

        verify(action).invoke()
        verifyNoMoreInteractions(action)
    }
}