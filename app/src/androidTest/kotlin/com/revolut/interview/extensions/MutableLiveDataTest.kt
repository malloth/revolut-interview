package com.revolut.interview.extensions

import androidx.test.annotation.UiThreadTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class MutableLiveDataTest {

    @Test
    @UiThreadTest
    fun createsMutableLiveDataWithGivenValue() {
        val expected = 123

        val result = mutableLiveDataOf(expected)

        assertEquals(expected, result.value)
    }

    @Test
    @UiThreadTest
    fun createsMutableLiveDataWithNullValue() {
        val result = mutableLiveDataOf<String>()

        assertNull(result.value)
    }
}