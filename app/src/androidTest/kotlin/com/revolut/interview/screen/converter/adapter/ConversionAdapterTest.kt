package com.revolut.interview.screen.converter.adapter

import android.text.SpannableStringBuilder
import com.nhaarman.mockitokotlin2.*
import com.revolut.interview.domain.currency.CurrencyEmojiProvider
import com.revolut.interview.extensions.setTextChangedListener
import com.revolut.interview.screen.converter.model.ConversionData
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

class ConversionAdapterTest {

    private val currencyEmojiProviderMock: CurrencyEmojiProvider = mock()

    private val tested = ConversionAdapter(currencyEmojiProviderMock)

    @Test
    fun returnsItemCountEqualToSetConversions() {
        val conversions = listOf(
            ConversionData("EUR", "123"),
            ConversionData("USD", "234"),
            ConversionData("PLN", "345")
        )

        tested.conversions = conversions

        assertEquals(conversions.size, tested.itemCount)
    }

    @Test
    fun returnsItemIdAsConversionCurrencyHashCode() {
        val currency = "EUR"
        val conversionData = ConversionData(currency, "123")
        val conversions = listOf(conversionData)

        tested.conversions = conversions
        val result = tested.getItemId(0)

        assertEquals(currency.hashCode().toLong(), result)
    }

    @Test
    fun setsCurrencyEmojiFlagWhenBindingViewHolder() {
        val conversions = listOf(ConversionData("EUR", "123"))
        val viewHolder = ConversionAdapter.ViewHolder(
            mock(), mock(), mock(), mock(), mock()
        )
        val position = 0
        val emoji = "\uD83C\uDDE6\uD83C\uDDFA"

        whenever(currencyEmojiProviderMock.getCurrencyEmoji(conversions[0].currency)).thenReturn(
            emoji
        )

        tested.conversions = conversions
        tested.bindViewHolder(viewHolder, position)

        verify(viewHolder.currencyIcon).text = emoji
    }

    @Test
    fun setsCurrencySymbolWhenBindingViewHolder() {
        val conversions = listOf(ConversionData("EUR", "123"))
        val viewHolder = ConversionAdapter.ViewHolder(
            mock(), mock(), mock(), mock(), mock()
        )
        val position = 0

        tested.conversions = conversions
        tested.bindViewHolder(viewHolder, position)

        verify(viewHolder.currencySymbol).text = conversions[position].currency
    }

    @Test
    fun setsCurrencyNameWhenBindingViewHolder() {
        val conversions = listOf(ConversionData("EUR", "123"))
        val viewHolder = ConversionAdapter.ViewHolder(
            mock(), mock(), mock(), mock(), mock()
        )
        val position = 0
        val currencyName = Currency.getInstance("EUR").displayName

        tested.conversions = conversions
        tested.bindViewHolder(viewHolder, position)

        verify(viewHolder.currencyName).text = currencyName
    }

    @Test
    fun setsInputAsEnabledWhenBindingViewHolderForFirstPosition() {
        val conversions = listOf(
            ConversionData("EUR", "123"),
            ConversionData("USD", "345")
        )
        val viewHolder = ConversionAdapter.ViewHolder(
            mock(), mock(), mock(), mock(), mock()
        )
        val position = 0

        tested.conversions = conversions
        tested.bindViewHolder(viewHolder, position)

        verify(viewHolder.inputValue).isEnabled = true
    }

    @Test
    fun requestsFocusForInputWhenBindingViewHolderForFirstPosition() {
        val conversions = listOf(
            ConversionData("EUR", "123"),
            ConversionData("USD", "345")
        )
        val viewHolder = ConversionAdapter.ViewHolder(
            mock(), mock(), mock(), mock(), mock()
        )
        val position = 0

        tested.conversions = conversions
        tested.bindViewHolder(viewHolder, position)

        verify(viewHolder.inputValue).requestFocus()
    }

    @Test
    fun setsListenerForInputWhenBindingViewHolderForFirstPosition() {
        val conversions = listOf(
            ConversionData("EUR", "123"),
            ConversionData("USD", "345")
        )
        val viewHolder = ConversionAdapter.ViewHolder(
            mock(), mock(), mock(), mock(), mock()
        )
        val position = 0

        tested.onInputTextChanged = mock()
        tested.conversions = conversions
        tested.bindViewHolder(viewHolder, position)

        verify(viewHolder.inputValue).setTextChangedListener(tested.onInputTextChanged)
    }

    @Test
    fun setsInputAsDisabledWhenBindingViewHolderForOtherPositions() {
        val conversions = listOf(
            ConversionData("EUR", "123"),
            ConversionData("USD", "345")
        )
        val viewHolder = ConversionAdapter.ViewHolder(
            mock(), mock(), mock(), mock(), mock()
        )
        val position = 1

        tested.conversions = conversions
        tested.bindViewHolder(viewHolder, position)

        verify(viewHolder.inputValue).isEnabled = false
    }

    @Test
    fun doesNotRequestFocusForInputWhenBindingViewHolderForOtherPositions() {
        val conversions = listOf(
            ConversionData("EUR", "123"),
            ConversionData("USD", "345")
        )
        val viewHolder = ConversionAdapter.ViewHolder(
            mock(), mock(), mock(), mock(), mock()
        )
        val position = 1

        tested.conversions = conversions
        tested.bindViewHolder(viewHolder, position)

        verify(viewHolder.inputValue, never()).requestFocus()
    }

    @Test
    fun clearsListenerForInputWhenBindingViewHolderForOtherPosition() {
        val conversions = listOf(
            ConversionData("EUR", "123"),
            ConversionData("USD", "345")
        )
        val viewHolder = ConversionAdapter.ViewHolder(
            mock(), mock(), mock(), mock(), mock()
        )
        val position = 1

        tested.conversions = conversions
        tested.bindViewHolder(viewHolder, position)

        verify(viewHolder.inputValue).setTextChangedListener(null)
    }

    @Test
    fun setsTextForInputWhenBindingViewHolder() {
        val conversions = listOf(
            ConversionData("EUR", "123"),
            ConversionData("USD", "345")
        )
        val viewHolder = ConversionAdapter.ViewHolder(
            mock(), mock(), mock(), mock(), mock()
        )
        val position = 0

        whenever(viewHolder.inputValue.text).thenReturn(SpannableStringBuilder.valueOf("888"))

        tested.conversions = conversions
        tested.bindViewHolder(viewHolder, position)

        verify(viewHolder.inputValue).setText("123")
    }

    @Test
    fun doesNotSetTheSameTextForInputWhenBindingViewHolder() {
        val conversions = listOf(
            ConversionData("EUR", "123"),
            ConversionData("USD", "345")
        )
        val viewHolder = ConversionAdapter.ViewHolder(
            mock(), mock(), mock(), mock(), mock()
        )
        val position = 0

        whenever(viewHolder.inputValue.text).thenReturn(SpannableStringBuilder.valueOf("123"))

        tested.conversions = conversions
        tested.bindViewHolder(viewHolder, position)

        verify(viewHolder.inputValue, never()).setText("123")
    }

    @Test
    fun setsClickListenerForItemWhenBindingViewHolder() {
        val conversions = listOf(
            ConversionData("EUR", "123"),
            ConversionData("USD", "345")
        )
        val viewHolder = ConversionAdapter.ViewHolder(
            mock(), mock(), mock(), mock(), mock()
        )
        val position = 0

        tested.conversions = conversions
        tested.bindViewHolder(viewHolder, position)

        verify(viewHolder.itemView).setOnClickListener(any())
    }
}