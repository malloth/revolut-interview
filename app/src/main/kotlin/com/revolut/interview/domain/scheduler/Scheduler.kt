package com.revolut.interview.domain.scheduler

interface Scheduler {

    fun scheduleRecurrent(frequency: Long, action: () -> Unit)

    fun unscheduleAll()
}