package com.revolut.interview.domain.scheduler.timer

import java.util.*

object TimerTaskFactory {

    fun create(action: () -> Unit): TimerTask = object : TimerTask() {

        override fun run() {
            action()
        }
    }
}