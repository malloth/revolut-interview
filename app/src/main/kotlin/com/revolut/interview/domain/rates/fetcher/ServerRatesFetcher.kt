package com.revolut.interview.domain.rates.fetcher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.revolut.interview.domain.rates.api.RatesApi
import com.revolut.interview.domain.rates.api.RatesApi.Companion.ratesApi
import com.revolut.interview.domain.rates.model.ConversionRates
import com.revolut.interview.domain.rates.model.ConversionRates.Companion.DEFAULT_CURRENCY
import com.revolut.interview.domain.scheduler.ActionScheduler
import com.revolut.interview.domain.scheduler.Scheduler
import com.revolut.interview.extensions.mutableLiveDataOf

class ServerRatesFetcher(
    private val api: RatesApi = ratesApi(),
    private val scheduler: Scheduler = ActionScheduler(),
    private val baseData: MutableLiveData<String> = mutableLiveDataOf(DEFAULT_CURRENCY),
    private val ratesData: MutableLiveData<ConversionRates> = mutableLiveDataOf(ConversionRates.EMPTY),
    private val ratesFetchingTask: () -> Unit = {
        RatesFetchingStrategy(
            api,
            baseData,
            ratesData
        )
    }
) : RatesFetcher {

    override val base: LiveData<String> = baseData

    override val rates: LiveData<ConversionRates> = ratesData

    override fun startFetching() {
        scheduler.scheduleRecurrent(FETCH_FREQUENCY_MS, ratesFetchingTask)
    }

    override fun stopFetching() {
        scheduler.unscheduleAll()
    }

    override fun setBase(base: String) {
        baseData.postValue(base)
    }

    private companion object {

        const val FETCH_FREQUENCY_MS = 1_000L
    }
}