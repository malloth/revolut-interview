package com.revolut.interview.domain.currency

interface CurrencyEmojiProvider {

    fun getCurrencyEmoji(currency: String): String
}