package com.revolut.interview.domain.scheduler

import com.revolut.interview.domain.scheduler.timer.TimerTaskFactory
import java.util.*

class ActionScheduler(
    private val timer: Timer = Timer(THREAD_NAME),
    private val timerTaskFactory: (() -> Unit) -> TimerTask = TimerTaskFactory::create
) : Scheduler {

    override fun scheduleRecurrent(frequency: Long, action: () -> Unit) {
        timer.scheduleAtFixedRate(timerTaskFactory(action), 0L, frequency)
    }

    override fun unscheduleAll() {
        timer.cancel()
    }

    private companion object {

        const val THREAD_NAME = "Scheduler"
    }
}