package com.revolut.interview.domain.rates.fetcher

import androidx.lifecycle.LiveData
import com.revolut.interview.domain.rates.model.ConversionRates

interface RatesFetcher {

    val base: LiveData<String>

    val rates: LiveData<ConversionRates>

    fun startFetching()

    fun stopFetching()

    fun setBase(base: String)
}