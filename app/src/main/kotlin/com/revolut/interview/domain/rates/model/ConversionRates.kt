package com.revolut.interview.domain.rates.model

data class ConversionRates(
    val base: String,
    val date: String,
    val rates: Map<String, Double>
) {

    companion object {

        const val DEFAULT_CURRENCY = "EUR"

        val EMPTY = ConversionRates(
            base = DEFAULT_CURRENCY,
            date = "",
            rates = emptyMap()
        )
    }
}