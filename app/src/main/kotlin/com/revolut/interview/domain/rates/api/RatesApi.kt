package com.revolut.interview.domain.rates.api

import com.revolut.interview.domain.rates.model.ConversionRates
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApi {

    @GET("/latest")
    fun getLatestRates(@Query("base") base: String): Call<ConversionRates>

    companion object {

        private const val SERVER_URL = "https://revolut.duckdns.org"

        fun ratesApi(): RatesApi = Retrofit.Builder()
            .baseUrl(SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(RatesApi::class.java)
    }
}