package com.revolut.interview.domain.rates.fetcher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.revolut.interview.domain.rates.api.RatesApi
import com.revolut.interview.domain.rates.model.ConversionRates
import com.revolut.interview.extensions.requireValue

object RatesFetchingStrategy :
        (RatesApi, LiveData<String>, MutableLiveData<ConversionRates>) -> Unit {

    override fun invoke(
        api: RatesApi,
        baseData: LiveData<String>,
        ratesData: MutableLiveData<ConversionRates>
    ) {
        try {
            val base = baseData.requireValue()
            val response = api.getLatestRates(base).execute()

            if (response.isSuccessful) {
                val conversionRates = response.body()

                if (conversionRates != null) {
                    ratesData.postValue(conversionRates)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}