package com.revolut.interview.screen.converter.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.revolut.interview.R
import com.revolut.interview.domain.rates.model.ConversionRates
import com.revolut.interview.screen.converter.adapter.ConversionAdapter
import com.revolut.interview.screen.converter.model.ConversionData
import com.revolut.interview.screen.converter.viewmodel.ConverterViewModel
import kotlinx.android.synthetic.main.fragment_converter.*

class ConverterFragment : Fragment() {

    private val conversionAdapter =
        ConversionAdapter()
    private val conversionObserver = Observer<List<ConversionData>> {
        conversionAdapter.conversions = it
    }
    private val ratesObserver = Observer<ConversionRates> {
        viewModel.onConversionRatesChanged()
    }
    private val viewModel by lazy {
        ViewModelProviders.of(this)[ConverterViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(viewModel) {
            conversions.observe(this@ConverterFragment, conversionObserver)
            rates.observe(this@ConverterFragment, ratesObserver)
        }

        with(conversionAdapter) {
            onConversionClick = viewModel::onConversionClicked
            onInputTextChanged = viewModel::onInputTextChanged
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_converter, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(list) {
            adapter = conversionAdapter
            setHasFixedSize(true)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        with(viewModel) {
            conversions.removeObserver(conversionObserver)
            rates.removeObserver(ratesObserver)
        }
    }
}