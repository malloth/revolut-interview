package com.revolut.interview.screen.converter.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.revolut.interview.R
import com.revolut.interview.domain.currency.CurrencyEmojiProvider
import com.revolut.interview.domain.currency.SystemCurrencyEmojiProvider
import com.revolut.interview.extensions.setTextChangedListener
import com.revolut.interview.screen.converter.adapter.ConversionAdapter.ViewHolder
import com.revolut.interview.screen.converter.model.ConversionData
import kotlinx.android.synthetic.main.listitem_currency.view.*
import java.util.*

class ConversionAdapter(
    private val currencyEmojiProvider: CurrencyEmojiProvider = SystemCurrencyEmojiProvider
) : RecyclerView.Adapter<ViewHolder>() {

    var conversions: List<ConversionData> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onConversionClick: (ConversionData) -> Unit = {}
    var onInputTextChanged: (String) -> Unit = {}

    init {
        setHasStableIds(true)
    }

    override fun getItemCount(): Int =
        conversions.size

    override fun getItemId(position: Int): Long =
        conversions[position].currency.hashCode().toLong()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.listitem_currency, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val conversion = conversions[position]

        with(holder) {
            currencyIcon.text = currencyEmojiProvider.getCurrencyEmoji(conversion.currency)
            currencySymbol.text = conversion.currency
            currencyName.text = Currency.getInstance(conversion.currency).displayName

            with(inputValue) {
                if (position == 0) {
                    isEnabled = true
                    requestFocus()
                    setTextChangedListener(onInputTextChanged)
                } else {
                    isEnabled = false
                    setTextChangedListener(null)
                }

                if (text?.toString() != conversion.value) {
                    setText(conversion.value)
                }
            }

            itemView.setOnClickListener {
                onConversionClick(conversion)
            }
        }
    }

    class ViewHolder(
        itemView: View,
        val currencyIcon: TextView = itemView.currency_icon,
        val currencySymbol: TextView = itemView.currency_symbol,
        val currencyName: TextView = itemView.currency_name,
        val inputValue: EditText = itemView.input
    ) : RecyclerView.ViewHolder(itemView)
}