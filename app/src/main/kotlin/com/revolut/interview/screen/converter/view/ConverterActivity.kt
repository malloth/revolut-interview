package com.revolut.interview.screen.converter.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.revolut.interview.R

class ConverterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
    }
}