package com.revolut.interview.screen.converter.model

data class ConversionData(val currency: String, val value: String)