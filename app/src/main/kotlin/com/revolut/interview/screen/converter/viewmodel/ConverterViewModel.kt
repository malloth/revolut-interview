package com.revolut.interview.screen.converter.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.revolut.interview.domain.rates.fetcher.RatesFetcher
import com.revolut.interview.domain.rates.fetcher.ServerRatesFetcher
import com.revolut.interview.domain.rates.model.ConversionRates
import com.revolut.interview.domain.rates.model.ConversionRates.Companion.EMPTY
import com.revolut.interview.extensions.mutableLiveDataOf
import com.revolut.interview.extensions.requireValue
import com.revolut.interview.screen.converter.model.ConversionData
import java.util.*

class ConverterViewModel(
    private val baseValueData: MutableLiveData<String> = mutableLiveDataOf(""),
    private val conversionData: MutableLiveData<List<ConversionData>> = mutableLiveDataOf(emptyList()),
    private val ratesFetcher: RatesFetcher = ServerRatesFetcher(),
    private val locale: Locale = Locale.getDefault()
) : ViewModel() {

    val conversions: LiveData<List<ConversionData>> = conversionData

    val rates: LiveData<ConversionRates> = ratesFetcher.rates

    init {
        ratesFetcher.startFetching()
    }

    override fun onCleared() {
        super.onCleared()

        ratesFetcher.stopFetching()
    }

    fun onConversionClicked(conversion: ConversionData) {
        baseValueData.value = conversion.value
        ratesFetcher.setBase(conversion.currency)
        swapConversions(conversion)
    }

    fun onInputTextChanged(input: String) {
        baseValueData.value = input
        updateConversions()
    }

    fun onConversionRatesChanged() {
        updateConversions()
    }

    private fun swapConversions(conversion: ConversionData) {
        val nc = conversionData.requireValue()
            .toMutableList()
            .apply {
                remove(conversion)
                add(0, conversion)
            }

        conversionData.postValue(nc)
    }


    private fun updateConversions() {
        val b = ratesFetcher.base.requireValue()
        val bv = baseValueData.requireValue().toDoubleOrNull() ?: 0.0
        val r = ratesFetcher.rates.requireValue()

        if (r == EMPTY)
            return

        val c = conversionData.requireValue().filter { it.currency != b }
        val nc = mutableListOf<ConversionData>()

        nc.add(ConversionData(b, baseValueData.requireValue()))

        if (c.isNotEmpty()) {
            c.forEach {
                val rate = r.rates[it.currency] ?: 1.0
                nc += ConversionData(it.currency, (bv * rate).toCurrencyFormat(it.currency))
            }
        } else {
            r.rates.forEach {
                nc += ConversionData(it.key, (bv * it.value).toCurrencyFormat(it.key))
            }
        }
        conversionData.postValue(nc)
    }

    private fun Double.toCurrencyFormat(currencySymbol: String): String {
        val currency = Currency.getInstance(currencySymbol)
        val fractionDigits = currency.defaultFractionDigits

        return String.format(locale, "%.${fractionDigits}f", this)
    }
}