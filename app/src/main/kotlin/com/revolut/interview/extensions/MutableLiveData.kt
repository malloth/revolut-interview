package com.revolut.interview.extensions

import androidx.lifecycle.MutableLiveData

fun <T> mutableLiveDataOf(value: T? = null): MutableLiveData<T> =
    MutableLiveData<T>().apply { this.value = value }