package com.revolut.interview.extensions

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.revolut.interview.R

fun EditText.setTextChangedListener(listener: ((String) -> Unit)?) {
    val oldWatcher = getTag(R.id.edit_text_listener) as? TextWatcher

    if (oldWatcher != null) {
        removeTextChangedListener(oldWatcher)
    }

    if (listener != null) {
        val newWatcher = object : TextWatcher {

            override fun afterTextChanged(s: Editable?) =
                listener(s.toString())

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) = Unit

            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) = Unit
        }
        addTextChangedListener(newWatcher)
        setTag(R.id.edit_text_listener, newWatcher)
    }
}