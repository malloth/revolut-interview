package com.revolut.interview.extensions

import androidx.lifecycle.LiveData

fun <T : Any> LiveData<T>.requireValue(): T = checkNotNull(value)